<?php

namespace MouTeam\FormJsValidationBundle\Service;

use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class FormValidation
{
    /** @var \Symfony\Component\Validator\Validator\ValidatorInterface */
    private $validator;

    /** @var \Symfony\Contracts\Translation\TranslatorInterface */
    private $translator;

    /** @var \MouTeam\FormJsValidationBundle\Service\MappingInterface */
    private $mapping;

    public function __construct(
        ValidatorInterface $validator,
        TranslatorInterface $translator,
        MappingInterface $mapping
    )
    {
        $this->validator = $validator;
        $this->mapping = $mapping;
        $this->translator = $translator;
    }

    public function addFormValidation(FormInterface $form): FormInterface
    {
        /** @var \Symfony\Component\Validator\Mapping\ClassMetadata $metadata */
        $metadata = $this->validator->getMetadataFor($form->getConfig()->getDataClass());
        $constrainedProperties = $metadata->getConstrainedProperties();
        $mapping = $this->mapping->getMapping();

        $validationGroups = $form->getConfig()->getOption('validation_groups');

        foreach ($form->all() as $field) {
            $name = $field->getConfig()->getName();
            $innerType = $field->getConfig()->getType()->getInnerType();

            if (in_array($name, $constrainedProperties)) {
                $type = get_class($innerType);
                $options = $field->getConfig()->getOptions();
                $constraints = $metadata->getPropertyMetadata($name)[0]->getConstraints();

                if (!isset($options["attr"])) {
                    $options["attr"] = [];
                }

                /** @var \Symfony\Component\Validator\Constraint $constraint */
                foreach ($constraints as $constraint) {
                    foreach ($validationGroups as $validationGroup) {
                        if (in_array($validationGroup, $constraint->groups)) {
                            $constraintName = ((new \ReflectionClass($constraint))->getShortName());

                            if (!isset($mapping[$constraintName])) {
                                continue;
                            }
                            $newAttrs = call_user_func_array($mapping[$constraintName], [$constraint, $this->translator]);
                            $options["attr"] = array_merge($options["attr"], $newAttrs);
                        }
                    }

                }
                if ($innerType instanceof RepeatedType) {
                    $firstOptions = isset($options["first_options"]) ? $options["first_options"] : [];
                    $options["first_options"] = $this->addRepeatedFieldJsValidation($mapping, $constraints, $field->get('first'), $firstOptions, $validationGroup);
                    $secondOptions = isset($options["second_options"]) ? $options["second_options"] : [];
                    $options["second_options"] = $this->addRepeatedFieldJsValidation($mapping, $constraints, $field->get('second'), $secondOptions, $validationGroup);
                }
                $form->add($name, $type, $options);
            }
        }

        return $form;
    }

    private function addRepeatedFieldJsValidation($mapping, $constraints, $field, $options, $validationGroup = "Default")
    {
        $options = $field->getConfig()->getOptions();
        if (!isset($options["attr"])) {
            $options["attr"] = [];
        }
        foreach ($constraints as $constraint) {
            if (in_array($validationGroup, $constraint->groups)) {
                $constraintName = ((new \ReflectionClass($constraint))->getShortName());
                if (!isset($mapping[$constraintName])) {
                    continue;
                }
                $newAttrs = call_user_func_array($mapping[$constraintName], [$constraint, $this->translator]);
                $options["attr"] = array_merge($options["attr"], $newAttrs);
            }
        }
        // Add a constraint to the second field in order to control the equality with the first one
        if ($field->getConfig()->getName() == 'second') {
            $options["attr"] = $this->addEqualToConstraint($field, $options["attr"]);
        }

        return $options;
    }

    protected function addEqualToConstraint($field, $attrOptions)
    {
        return $attrOptions;
    }
}
