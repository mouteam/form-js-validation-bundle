<?php

namespace MouTeam\FormJsValidationBundle\Service;

use Symfony\Contracts\Translation\TranslatorInterface;

class FormValidationIoMapping implements MappingInterface
{
    /** @var \Symfony\Contracts\Translation\TranslatorInterface */
    private $translator;

    /**
     * @param \Symfony\Contracts\Translation\TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    public function getMapping(): array
    {
        $mapping = [
            "NotBlank" => function ($constraint) {
                return [
                    "data-fv-not-empty"           => "true",
                    "data-fv-not-empty___message" => $this->translator->trans($constraint->message, [], 'validators'),
                ];
            },
            "Email"    => function ($constraint) {
                return [
                    "data-fv-email-address"           => "true",
                    "data-fv-email-address___message" => $this->translator->trans($constraint->message, [], 'validators'),
                ];
            },
        ];

        return $mapping;
    }
}
