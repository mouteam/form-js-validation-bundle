<?php

namespace MouTeam\FormJsValidationBundle\Service;

interface MappingInterface
{
    public function getMapping(): array;
}
