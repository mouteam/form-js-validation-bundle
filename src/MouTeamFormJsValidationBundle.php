<?php

namespace MouTeam\FormJsValidationBundle;

use MouTeam\FormJsValidationBundle\DependencyInjection\MouTeamFormJsValidationExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MouTeamFormJsValidationBundle extends Bundle
{
    /**
     * @return \MouTeam\FormJsValidationBundle\DependencyInjection\MouTeamFormJsValidationExtension
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            return new MouTeamFormJsValidationExtension();
        }

        return $this->extension;
    }
}
