<?php

namespace MouTeam\FormJsValidationBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class MouTeamFormJsValidationExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');


        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);


        $definition = $container->getDefinition('mouteam_form_js_validation.form_validation');

        if (null !== $config['form_validation_mapping']) {
            $definition->setArgument(3, $config['form_validation_mapping']);
        }
    }

    /**
     * Replace default name mou_team_form_js_validation
     *
     * @return string
     */
    public function getAlias()
    {
        return 'mouteam_form_js_validation';
    }
}
