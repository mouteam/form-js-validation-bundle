<?php

namespace MouTeam\FormJsValidationBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('mou_team_form_js_validation');

        if (method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            $rootNode = $treeBuilder->root('mou_team_form_js_validation');
        }

        $rootNode
            ->children()
                ->scalarNode('form_validation_mapping')->defaultNull()->end();

        return $treeBuilder;
    }
}
